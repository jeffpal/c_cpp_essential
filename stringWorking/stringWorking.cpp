#include "iostream"
//avoid the std::cout declaration
using namespace std;

class pessoa
{

public:
		string name, address, profession;
		int age;

};

int main(){

	pessoa jefferson;

	// according with my tests the """ getline(cin, str) """ declaration worked just with string type 
	// where str is the variable and the rest is pattern
	cout << "\nput your name: \n";
	getline(cin, jefferson.name);
	cout << "\nput your address: \n";
	getline(cin, jefferson.address);
	cout << "\nput your profession: \n";
	getline(cin, jefferson.profession);
	cout << "\nput your age: \n";
	cin >> jefferson.age;

	

	cout << "\nYour name is: "  << jefferson.name <<"\n";
	cout << "\nYour address is: "  << jefferson.address <<"\n";
	cout << "\nYour profession is: " << jefferson.profession <<"\n";
	cout << "\nYour age is: " << jefferson.age <<"\n";
}


