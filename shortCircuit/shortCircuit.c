#include <stdio.h>

void main(){

	int valorA[5]={-5,-5,-5,5,5}, valorB[5]={-10,10,0,10,0};
	int i;

	printf("\nOperador lógico &&\n----------------------------\n");
	for(i=0; i<5; i++){

		if(valorA[i] && valorB[i]){
			printf("%d && %d: TRUE \n", valorA[i], valorB[i]);
		}

		else printf("%d && %d: FALSE \n", valorA[i], valorB[i]);

	}

	printf("\nOperador lógico &\n----------------------------\n");
	for(i=0; i<5; i++){
		if(valorA[i] & valorB[i]){
			printf("%d & %d: TRUE \n", valorA[i], valorB[i]);
		}

		else printf("%d & %d: FALSE \n", valorA[i], valorB[i]);

	}
}