#include "iostream"
using namespace std;

class person{

	string name, address;
	int age;
	bool gender;

	public:

		string get_name();
		string get_address();
		int get_age();
		bool get_gender();

		void set_name(string);
		void set_address(string);
		void set_age(int);
		void set_gender(bool);

		void show_name();
		void show_address();
		void show_age();
		void show_gender();

};