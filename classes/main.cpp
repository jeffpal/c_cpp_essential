#include "classes.h"

int main()
{
	person jefferson;

	jefferson.set_name("Jefferson Palheta");
	jefferson.set_address("Belém-Pa");
	jefferson.set_age(20);
	jefferson.set_gender(true);

	jefferson.show_name();
	jefferson.show_address();
	jefferson.show_age();
	jefferson.show_gender();

	cout << "\nthe name is: " << jefferson.get_name() <<"\n";
	cout << "\nthe address is: " << jefferson.get_address() <<"\n";
	cout << "\nthe age is: " << jefferson.get_age() <<"\n";

	if (jefferson.get_gender()==true)
	cout << "\nthe gender is Male\n";
	
	else
		cout << "\nthe gender is Female\n";

	return 0;
}

