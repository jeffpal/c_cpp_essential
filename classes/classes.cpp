#include "classes.h"		


string person::get_name()
{
	return name;
}


string person::get_address()
{
	return address;
}

int person::get_age()
{
	return age;
}

bool person::get_gender()
{
	return gender;
}




void person::set_name(string setname)
{
	name = setname;
}

void person::set_address(string setaddress)
{
	address = setaddress;
}

void person::set_age(int setage)
{
	age = setage;
}

void person::set_gender(bool setgender)
{
	gender = setgender;
}





void person::show_name()
{
	cout << "\nthe name is: " << name <<"\n";
}

void person::show_address()
{
	cout << "\nthe address is: " << address <<"\n";	
}

void person::show_age()
{
	cout << "\nthe age is: " << age <<"\n";
}

void person::show_gender()
{
	if (gender==true)
	cout << "\nthe gender is Male\n";

	else
	cout << "\nthe gender is Female\n";		
}