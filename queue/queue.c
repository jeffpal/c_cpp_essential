#include <stdio.h>
#include <stdlib.h>
#include <sys/queue.h>
#include <pthread.h>
#include <string.h>
//------------------ESTRUTURA DE ENTRADA DA FILA------------------------
typedef struct QUEUE_ENTRY{
    char * element;
    STAILQ_ENTRY(QUEUE_ENTRY) next; //ponteiro para o próximo elemento da fila
 
}QUEUE_ENTRY;


//------------------ESTRUTURA DE ARGUMENTOS PARA AS THREADS------------------------
typedef struct{

    QUEUE_ENTRY *new_element;
      
}thread_arg;


pthread_mutex_t available_item_mutex, queue_mutex;
static int available_item=0;
STAILQ_HEAD(HEAD_QUEUE_ESTRUCTURE, QUEUE_ENTRY); //define uma estrutura head que contém um ponteiro simples que aponta para o primeiro elemento da fila
typedef struct HEAD_QUEUE_ESTRUCTURE headQueue;

headQueue myQueue = STAILQ_HEAD_INITIALIZER(myQueue);


//------------------------PROTÓTIPO DAS FUNÇÕES------------------------------------
void create_element(QUEUE_ENTRY **elementp);
void push(QUEUE_ENTRY **new_elementp, headQueue *queue);
void *insertElementsOnMyQueue(void *inputArg);
void *removeElementsOfQueue(void *inputArg2);

int main(int argc, char *argv[]){

	thread_arg myThreadArg;
	pthread_t threads[2];

	STAILQ_INIT(&myQueue);
	create_element(&myThreadArg.new_element);
	STAILQ_INSERT_HEAD(&myQueue, myThreadArg.new_element, next);

	pthread_mutex_init(&available_item_mutex, NULL);
	pthread_mutex_init(&queue_mutex, NULL);
	pthread_create(&threads[0], NULL, insertElementsOnMyQueue, (void *)&myThreadArg);
	pthread_create(&threads[1], NULL, removeElementsOfQueue, NULL);
    pthread_join(threads[0], NULL);
    pthread_join(threads[1], NULL);
    pthread_exit(NULL);
	
	return 0;

}

void push(QUEUE_ENTRY **new_elementp, headQueue *queue){

    create_element(new_elementp);
    STAILQ_INSERT_TAIL(queue, *new_elementp, next);


}


void create_element(QUEUE_ENTRY **elementp) {
        *elementp = (QUEUE_ENTRY *)malloc(sizeof(QUEUE_ENTRY));
        
}

void *insertElementsOnMyQueue(void *inputArg){

	thread_arg *myArgs=(thread_arg *)inputArg;

	unsigned int i=0;

	 while(i<50){

		myArgs->new_element->element=(char *)malloc(20*sizeof(char));
		printf("inserindo elemento %d\n", i);
		sprintf(myArgs->new_element->element, "elemento %d", i);
		push(&myArgs->new_element, &myQueue);
		available_item++;
		i++;
	 }
}

void *removeElementsOfQueue(void *inputArg2){
	QUEUE_ENTRY *temp;		
	int status;
	while(1){

		pthread_mutex_lock(&available_item_mutex);
	    status=available_item;
	    pthread_mutex_unlock(&available_item_mutex);

		if(status>0){

			

			pthread_mutex_lock(&queue_mutex);
            temp=STAILQ_FIRST(&myQueue);
            pthread_mutex_unlock(&queue_mutex);

            printf("Removendo %s\n", temp->element); 

	        pthread_mutex_lock(&queue_mutex);
            STAILQ_REMOVE(&myQueue, temp, QUEUE_ENTRY, next);
            pthread_mutex_unlock(&queue_mutex);
            free(temp);

            pthread_mutex_lock(&available_item_mutex);
		    available_item--;
		    pthread_mutex_unlock(&available_item_mutex);
        }
      }
}