#include <stdio.h>
#include <string.h>
#include <pthread.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

typedef struct{
	char bufferPing[10], bufferPong[10];
	int estadoPing, estadoPong;
	pthread_mutex_t printf;
}argumentos_t;

void *captura(void *arg1);
void *processa(void *arg);

char ping[10], pong[10];

int main (int argc, char *argv[]){
	
	argumentos_t args;

	args.estadoPing=0;
	args.estadoPong=0;
	pthread_mutex_init(&args.printf, NULL);
	pthread_t threads[3];
	pthread_create(&threads[0], NULL, captura, (void *)&args);
	pthread_create(&threads[1], NULL, processa, (void *)&args);
	pthread_join(threads[0], NULL);
	pthread_join(threads[1], NULL);

return 0;
}



void *captura(void *arg1){
	
	argumentos_t *dados= (argumentos_t *)arg1;
	
	while(1){
		if(dados->estadoPing==0){
			strcpy(dados->bufferPing, "ping");	
			//strcpy(dados->bufferPing, "ping");
			printf("Inserindo no " ANSI_COLOR_YELLOW "ping"ANSI_COLOR_RESET"\n");
			pthread_mutex_lock(&dados->printf);
			dados->estadoPing=1;
			pthread_mutex_unlock(&dados->printf);
		}
		if(dados->estadoPong==0){
			strcpy(dados->bufferPong, "pong");
			printf("Inserindo no " ANSI_COLOR_GREEN"pong"ANSI_COLOR_RESET"\n"); 		
			pthread_mutex_lock(&dados->printf);
			dados->estadoPong=1;
			pthread_mutex_unlock(&dados->printf);	
		
		}
	
	}
}

void *processa(void *arg2){
	argumentos_t *dados=(argumentos_t *)arg2;
	
	//strcpy(dados->bufferPong, "pong");
	while(1){
		if(dados->estadoPing==1){
			fflush(stdout);
			printf("Processando do "ANSI_COLOR_YELLOW"%s"ANSI_COLOR_RESET"\n", dados->bufferPing);
			pthread_mutex_lock(&dados->printf);
			dados->estadoPing=0;
			pthread_mutex_unlock(&dados->printf);
		}
		else if(dados->estadoPong==1){
			fflush(stdout);
			printf("Processando do "ANSI_COLOR_GREEN"%s"ANSI_COLOR_RESET"\n____________________\n\n", dados->bufferPong);
			pthread_mutex_lock(&dados->printf);
			dados->estadoPong=0;
			pthread_mutex_unlock(&dados->printf);
		}
	
	}
}
