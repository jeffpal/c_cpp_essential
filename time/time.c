#include <stdio.h>
#include <time.h>

int main(){

time_t t = time(NULL);
struct tm *time = localtime(&t);
printf("Current time: %d:%d:%d\n", time->tm_hour, time->tm_min, time->tm_sec);

return 0;
}
