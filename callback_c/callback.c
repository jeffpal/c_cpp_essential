#include<stdio.h>

void getCallBack(char buf[], void(*executeCallBack)(char[])){
    printf("callback recived\n");
    executeCallBack(buf);
}

void callBack(char buf[]){
    printf("executing callback...\n");
    printf("buf content: ");
    printf("%s\n", buf);
}

void main() {

    FILE *ReadingXmlFile = fopen("texto.rec", "r");
    char buf[10000];
    fread(buf, 1, 10000, ReadingXmlFile);
    fclose(ReadingXmlFile);

    printf("passing \"&callBack\"\n");
    getCallBack(buf, &callBack);

    printf("passing \"callBack\"\n");
    getCallBack(buf, callBack);
    //
}
