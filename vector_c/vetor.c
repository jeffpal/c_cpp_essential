#include<stdio.h>

printVector(char vetorChar1[], char* vetorChar2, int vetorInteiro1[], int* vetorInteiro2, int* valorInteiro){

	printf("\nargumento 1:  %s\n", vetorChar1);
	printf("argumento 2:  %s\n", vetorChar2);
	
	printf("------------------------------------\n");

	int i;
	for(i=0; i<=9; i++){
		printf("argumento 3 | vetorInteiro3[%d]:  %d\n", i, vetorInteiro1[i]);
	}

	printf("------------------------------------\n");
	
	for(i=0; i<=9; i++){
		printf("argumento 4 | vetorInteiro3[%d]:  %d\n", i, vetorInteiro2[i]);
	}
	
	printf("------------------------------------\n");	
	printf("argumento 5: %d\n",*valorInteiro);
	
}


void main(){

char vetorChar[13] = {"vetor de char"};
int vetorInteiro[10] = {1,2,3,4,5,6,7,8,9,10};
int valorInteiro = 10;

printVector(vetorChar, vetorChar, vetorInteiro, vetorInteiro, &valorInteiro);

}	
