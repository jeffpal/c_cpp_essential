#include<stdio.h>

void funcao1(int *p);

void main(){

int val1=10, val2=20, *p1=&val1;

printf("CASO 1: passando um ponteiro para um ponteiro\n__________________________________________________________________\n");
funcao1(p1);
printf("CASO 2: passando o endereco de uma variavel para um ponteiro\n__________________________________________________________________\n");
funcao1(&val2);

}


void funcao1(int *p){

	printf("endereço para onde o ponteiro p aponta: %p\n", p);
	printf("valor do endereço para onde o ponteiro p aponta: %p\n\n\n", p);
}



